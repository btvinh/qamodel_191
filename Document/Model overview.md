# Giới thiệu về mô hình Hỏi-đáp(Question-Answering)

Truy vấn dữ liệu từ một ngữ cảnh có sẵn là một vấn đề rất đáng để quan tâm trong lĩnh vực **Xử lý ngôn ngữ tự nhiên**. Đây là bài toán tập trung vào việc áp dụng **Deep learning** để giải quyết việc trả lời một câu hỏi cho một đoạn văn bản cụ thể. Và trong bài viết này nhóm em sẽ mô tả về mô hình **QANet** để hiểu được một cách tổng quan nhất về bài toán này.


## Mô hình chung(The model)

Đây là một mô hình feedforward sử dụng mạng nơ-ron tích chập(CNNs) để mã hóa và trích xuất các đặc trưng của và kỹ thuật self-attention. Trước khi đi vào bài toán ta sẽ quy ước một số kí hiệu và mục đích của bài toán:
* Một văn bản với n từ: C = {c1, c2,..., cn}
* Một câu hỏi với m từ về dữ kiện đã có trong đoạn văn bản trên: Q = {q1, q2,  ..., qm}
* Câu trả lời của mô hình dự đoán được: A = {ck, ck+1,...cj} là từ hoặc cụm    từ có trong C

![The QANet model](https://miro.medium.com/max/1608/1*AJZICMvoPbn-x_dTIy2L4A.png)
Mô hình gồm có chi tiết 5 lớp:
### **1. Input Embeeding Layer**
Ta nhúng mỗi từ ***w*** bằng cách nối vecto nhúng của nó ở mức từ và mức kí tự. Vecto nhúng ở mức từ được cố định trong quá tình đào tạo và khởi tạo lấy từ kết quả đã huấn luyện của GloVe với số chiều là $`p_{1}`$. Những từ không tìm thấy trong GloVe thì được gọi gán là \<OOV>(out-of-vocabulary).

Đối với vecto nhúng mỗi từ ngữ ở mức kí tự, ta biểu diễn mỗi kí tự dưới dạng một vecto có thể huấn luyện với số chiều là $`p_{2}`$, điều này có nghĩa là chúng ta có thể xem mỗi vecto đại diện cho mỗi từ ngữ là sự kết hợp giữa nhiều vecto kí tự với nhau. Và số lượng kí tự mỗi từ thì được mở rộng hoặc cắt ngắn bớt để chúng luôn cố định ở mức là 16 kí tự. Bây giờ ta đã có một ma trận kích thước là $`p_{2}*16`$. Ta lấy giá trị lớn nhất của mỗi hàng để lấy giá trị cho vecto biểu diễn của mỗi từ. Cuối cùng output của một từ *x* qua lớp này là sự nối 
$`[x_{w},x_{c}] \in R^{p1+p2}`$
với $`x_{w}`$ và $`x_{c}`$ tương ứng là vecto biểu diễn ở mức từ và mức kí tự của từ ngữ *x*.

### **2. Embedding Encoder Layer**
Lớp này là một chồng của các khối xây dựng cơ bản: [convolution-layer $`*`$ # + self-attention-layer + feed-forward-layer] như hình minh họa bên trên. Mạng nơ-ron tích chập được lấy với số kernel là 7, số lượng filter là d = 128, và số lượng lớp tích chập trong một khối này là 4. Với Self-Attention, người ta sử dụng Multi-head Attention. Với mỗi vị trí đầu vào(Query), ta sẽ tính tổng trọng số của các vị trí khác(Keys), dựa vào sự tương đồng giữa Query và Key được tính bằng tích vô hướng của 2 vecto biểu diễn của chúng. Số *head* được dùng là 8.

Mỗi một trong 3 lớp cơ bản trên đều được đặt trong một *residual block*. Với mỗi input *x* và một phép toán *f* thì đầu ra sẽ là *f(layernorm(x))+x*, nghĩa là có một đường truyền thông tin đầy đủ từ input đến ouput. Số lượng khối mã hóa là 1. Như vậy đầu vào của tầng này là một vecto có chiều là $`p_1 + p_2`$ ứng với mỗi từ, và ngay lập tức nó sẽ được áp xạ thành có kích thước d = 128 bới mạng nơ-ron tích chập. Đầu ra tương ứng sẽ có kích thước d = 128.

### **3. Context-Query Attention Layer**
Lớp này được xem như module tiêu chuẩn của các bài toán Question-Answering. Ta có $`C \in R^{d \times n}`$ và $`Q \in R{d \times m}`$ biểu diễn cho sự mã hóa của đoạn văn bản và câu hỏi. 

* Context-to-query Attention sẽ được xây dựng như sau: đầu tiên ta sẽ tính toán sự tương đồng giữa từng cặp từ trong C và Q, được biểu diễn bằng một Similarity Matrix $`S \in R^{n \times m}`$. Và mức độ tương đồng giữa từ thứ i trong đoạn văn bản và từ thứ j của câu hỏi được tính bằng công thức sau:

$`S_{ij} = W_o \times [ C_{:i};Q_{:j};C_{:i} \circ Q_{:j} ]`$

Với W là một trọng số học được trong quá trình huấn luyện, $`\circ`$ là phép nhân element-wise. Sau đó ta sẽ tính dùng softmax fuction để tính cho từng hàng của ma trận S:

![SM-C2Q](./images/SM-C2Q.png)

Màu đỏ càng đậm thì càng thể hiện sự tương quan càng lớn giữa từ trong đoạn văn bản và trong câu hỏi. Tiếp đó mỗi từ trong đoạn văn bản sẽ được biểu diễn bằng một vecto thể hiện sự hướng tập trung đến câu hỏi $`Q'_{:i}} = \sum_i a_ij Q_{:i}`$ với tất cả từ i trong đoạn văn bản thể hiện sự ảnh hưởng của câu hỏi lên từ thứ i.

* Query-to-context Attention: nhiệm vụ của lớp này là đánh dấu từ nào trong đoạn văn có độ tương tự cao nhất với từng từ trong câu hỏi và do đó nó có khả năng là câu trả lời cao nhất. Trọng số thể hiện độ hướng sự tập trung trên các từ ngữ trong đoạn văn $`b = softmax(max_{row}(S)) \in R^n`$ với ý nghĩa là sẽ lấy giá trị softmax lớn nhất trong các giá trị trong vecto từ thứ i. Tiếp đến ta nhân mỗi giá trị $`b_i`$ với vecto biểu diễn từ thứ i này trong ma trận C ta được $`Q'`$.

![Softmax](./images/softmax.PNG)

Cuối cùng ta kết hợp các ma trận này với nhau để được ma trận G mà mỗi cột i tại đó thể hiện sự ảnh hưởng của câu hỏi lên từ ngữ trong đoạn văn

$`G_{:i} = \beta(C_{:i},Q'_{:i},C'_{:i}) \in R^4d`$ với $`\beta = (c,q',c') = [c;q';c \circ q']`$

### **4. Model Encoder Layer**
Đầu vào của lớp này là ma trận G nói trên. Ma trận này sẽ được huân luyện qua 3 tầng các khối mã hóa. Đầu ra mỗi tầng ta thu được một ma trận thể hiện đoạn văn, kí hiệu lần lượt là $`M_1,M_2,M_3`$. Ta sẽ ghép nối $`M_1`$ và $`M_2`$ để làm đầu vào cho lớp tiếp theo tính toán ra vị trí bắt đầu của câu hỏi, tương tự ghép nối $`M_1`$ và $`M_3`$ để tính toán ra vị trí kết thúc của câu hỏi.

### **5. Output Layer

Ta dự đoán vị trí bắt đầu và kết thúc của câu trả lời trong đoạn văn bản như sau:

$`p^{(1)} = softmax(w_{p^{(1)}}^T [M_1,M_2])`$

$`p^{(2)} = softmax(w_{p^{(2)}}^T [M_1,M_3])`$

Với $`w_{p^{(1)}}^T \in R^{8d},w_{p^{(2)}}^T \in R^{8d}`$ là vecto trọng số sẽ được học trong lúc huấn luyện. Ta sẽ có hàm lỗi như sau:

$`L(\theta) = \frac{1}{N} \sum_i^N [log(p_{y_i^1}^1) + log(p_{y_i^2}^2)]`$

trong đó $`\theta`$ là bộ trọng số cần huấn luyện. $`y_i^1, y_i^2`$ là vị trí thực tế bắt đầu và kết thúc của câu trả lời, 2 giá trị này càng cao thì khả năng dự đoán càng chính xác.

## Kết quả kiểm thử:

Theo như trong bài luận văn mà nhóm em tham khảo[^1] thì ta có bảng đánh giá kết quả so với mô hình gốc trong bài báo[^2] mà luận văn tham khảo như sau:

![EM-F1](./images/EM-F1.PNG)

Thư viện sử dụng trong bài trên là Pytorch với tập dữ liệu là SQuAD dataset

Dưới đây là kết quả được lấy từ bài báo gốc[^2] với tập dữ liệu là SQuAD dataset:


![rs](./images/QANet-result.PNG)

## Tham khảo
[^1]: Luận văn "Xây dựng hệ thống hỏi đáp" của nhóm Nguyễn Viết Sang(1512798) và Trần Ngọc Quý(1512752).

[^2]: QANet: Combining Local Convolution with Global Self-Attention for Reading Comprehension,
Adams Wei Yu, David Dohan, Minh-Thang Luong, Rui Zhao, Kai Chen, Mohammad Norouzi, Quoc V. Le [link](https://arxiv.org/abs/1804.09541)


